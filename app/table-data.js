import { tests } from './data';
import _ from 'lodash';

// Redesign each assignment into a more managable object by turning methods into an array
const rebuildMethodsToArray = ({ methods, name }) => ({ name, methods: _.values(methods) });

const assignmentsObject = _.mapValues(tests, rebuildMethodsToArray);

const tableData = _.values(assignmentsObject);

export default tableData;
